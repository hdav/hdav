import { h, Component } from 'preact';
import './public/css/App.css';
import '@ajusa/lit/dist/lit.css';
import '@ajusa/lit/dist/util.css';


// import components
import Navbar from './components/nav/navbar';
import Home from './Home';
import Footer from './components/footer';
import Video from './components/video';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <Video />
        <Home />
        <Footer />
      </div>
    );
  }
}

export default App;

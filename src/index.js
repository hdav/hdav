import 'preact/devtools';
import { h, render } from 'preact';
import {createStore,compose} from 'redux';
import {Provider} from 'preact-redux';


import './public/css/index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

// import action
//import {setLanguage} from './actions/UserConfig';

// import root reducer
import rootReducer from './reducers';

// redux store
const store = createStore(rootReducer,compose(window.devToolsExtension ? window.devToolsExtension() : f=>f));

render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();

import {SET_SHOW_VIDEO} from './types';

export const setWatchVid = vidid=>{
    return {
        type: SET_SHOW_VIDEO,
        vidid
    }
}
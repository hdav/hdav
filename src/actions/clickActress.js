import {SET_FILTER_ACTRESS} from './types';

export const setFilterActress = (actressid,actresschn,actresseng)=>{
    return {
        type: SET_FILTER_ACTRESS,
        actressid,actresschn,actresseng
    }
}

import {SET_FILTER_TAG} from './types';

export const setFilterTag = (tagid,tagchn,tageng)=>{
    return {
        type: SET_FILTER_TAG,
        tagid,tagchn,tageng
    }
}

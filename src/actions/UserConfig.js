// sets user config settings in redux store and cookies
// storing language (and possibly user minimised videos)

import {SET_LANGUAGE} from './types';

export function setLanguage(language){
    return {
        type : SET_LANGUAGE,
        language
    }
}
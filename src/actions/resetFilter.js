import {RESET_FILTER} from './types';

export const resetFilter = ()=>{
    return {
        type: RESET_FILTER
    }
}

import { h, Component } from 'preact';

// import components
import Slider from './components/slider';

class Home extends Component{

    componentWillMount(){}
    render(){
        return(
            <div className="Home" id="home">
                <Slider />
            </div>
        );
    }
}

export default Home;

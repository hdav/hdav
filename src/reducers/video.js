import {SET_SHOW_VIDEO} from '../actions/types';

const initialState={
    videoID: ''
}

export default(state = initialState,action={})=>{
    switch(action.type){
        case SET_SHOW_VIDEO:
            return{
                videoID: action.vidid
            }
        default:
            return state;
    }
}
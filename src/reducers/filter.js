import {SET_FILTER_TAG,SET_FILTER_ACTRESS,RESET_FILTER} from '../actions/types';

const initialState={
    filterID: '',
    actressID: '',
    chn: '',
    eng: '',
    type: ''
}

export default(state = initialState,action={})=>{
    switch(action.type){
        case SET_FILTER_TAG:
            return{
                filterID: action.tagid,
                chn: action.tagchn,
                eng: action.tageng,
                type: 'tag'
            }
        case SET_FILTER_ACTRESS:
            return{
                filterID: action.actressid,
                chn: action.actresschn,
                eng: action.actresseng,
                type: 'actress'
            }
        case RESET_FILTER:
            return initialState;
        default:
            return state;
    }
}

import {SET_LANGUAGE} from '../actions/types';

const initialState={
    language:'chn'
}

export default(state = initialState,action={})=>{
    switch(action.type){
        case SET_LANGUAGE:
            return{
                language: action.language
            }
        default:
            return state;
    }
}
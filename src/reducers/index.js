import {combineReducers} from 'redux';


// import reducers
import language from './language';
import video from './video';
import filter from './filter';


export default combineReducers({
    language,
    video,
    filter
});
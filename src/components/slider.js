/*
    main component that shows the list of videos
*/

import { h, Component } from 'preact';
import {connect} from 'preact-redux';
import axios from 'axios';
import RSlider from 'react-rangeslider';

import 'react-rangeslider/lib/index.css';

import {setWatchVid} from '../actions/clickVid';
import {setFilterTag} from '../actions/clickTag';
import {setFilterActress} from '../actions/clickActress';
import {resetFilter} from '../actions/resetFilter';

class Slider extends Component{
    constructor(){
        super();
        this.state={
            videos: {},
            page: 1,
            next: true,
            back: false,
            lastPage: 0,
            fetching: false,
            showPageSlider: false
        }
        this.handleNextBtn = this.handleNextBtn.bind(this);
        this.handleBackBtn = this.handleBackBtn.bind(this);
    }
    componentWillMount(){
        this.setState({fetching:true});
        this.fetchVideos(1);
    }

    componentDidUpdate(prevProps){
        // changed tag filter / actress search
        if(prevProps !== this.props){
            // go back to 1st page and re-fetch videos list + last page number
            this.setState({fetching:true,page:1});
            this.fetchVideos(1);
            
        }

    }


    fetchVideos(page){
        /* --
        - first set fetching state to true
        - then check which method (tags or actress) user is filtering
        - fetch videos by sending get request to the backend
        - also fetch the last page number of this filter by sending another get request to the backend
        - finally, update the state of videos and last page number
        - set fetching state back to false to display the results
        -- */



        // fetch Videos
        this.setState({fetching:true});


        // check if user is trying to filter using tags
        this.props.filter.type === "tag" ?
        axios.get('https://hdav.pl/api/videosList?tagID='+this.props.filter.filterID+'&page='+page).then(res=>{
            // check last page
            axios.get('https://hdav.pl/api/lastPage?tagID='+this.props.filter.filterID).then(res2=>{
                this.setState({videos: res.data,lastPage: res2.data.pages,fetching: false});
                //console.log(this.state);//debug
            });
        })
        :
        // not filtering using tags so check if filtering actress
        this.props.filter.type === "actress" ?
        axios.get('https://hdav.pl/api/videosList?actressID='+this.props.filter.filterID+'&page='+page).then(res=>{
            // check last page
            axios.get('https://hdav.pl/api/lastPage?actressID='+this.props.filter.filterID).then(res2=>{
                this.setState({videos: res.data,lastPage: res2.data.pages,fetching: false});
                //console.log(this.state);//debug
            });
        })
        :
        // user not filtering using tags, so just fetch all videos
        axios.get('https://hdav.pl/api/videosList?page='+page).then(res=>{
            // check last page
            axios.get('https://hdav.pl/api/lastPage').then(res2=>{
                this.setState({videos: res.data,lastPage: res2.data.pages,fetching: false});
                //console.log(this.state);//debug
            });
        });
    }

    // prints 3x3 video list grid
    printVidGrid(){
        let vidsPerPage = Object.keys(this.state.videos).length; // 9
        let vidsPerRow = 3;
        let rowsPerPage = 3;//vidsPerPage / vidsPerRow
        let vids = [];
        for(let i=0;i<vidsPerPage;i++){
            //const video = this.state.videos[((this.state.page-1)*vidsPerPage)+i];
            const video = this.state.videos[i];
            //console.log(video);//debug
            vids.push(video);
        }
        //console.log(vids);//debug


        return Array.apply(null,Array(rowsPerPage)).map((obj,i)=>(
            <div className="row card noPadding noborder">

                {Array.apply(null,Array(vidsPerRow)).map((obj2,i2)=>(
                    vids[i*vidsPerRow+i2] ?
                    <div className="4 col card noborder vidCard" vidid={vids[i*vidsPerRow+i2].id} onClick={()=>this.handleClickVideo(vids[i*vidsPerRow+i2].id)}>
                        <h4>{vids[i*vidsPerRow+i2].idcode}</h4>
                        <img className="w-100" alt={vids[i*vidsPerRow+i2].idcode} src={vids[i*vidsPerRow+i2].picURL} />
                        <b>女優: </b>{vids[i*vidsPerRow+i2].actress.length > 1 ?
                                // multipl actress  
                                this.props.language === "chn" ? "多位演員" : "Multiple Actresses"
                            :
                                // only 1 actress
                                this.props.language === "chn" ? vids[i*vidsPerRow+i2].actress.chn[0] : vids[i*vidsPerRow+i2].actress.eng[0]
                            }
                    </div> :''
                ))}
            </div>
       ))



    }

    handleNextBtn(e){
        this.setState({page : this.state.page+1,fetching:true});
        this.fetchVideos(this.state.page);
    }

    handleBackBtn(e){
        this.setState({page : this.state.page-1,fetching:true});
        this.fetchVideos(this.state.page);
    }

    handleOnChange = e =>{
        this.setState({page :e });
    }

    handleComplete = e =>{
        this.setState({showPageSlider:false});
        this.fetchVideos(this.state.page);
    }

    handleClickPage = e =>{
        this.setState({showPageSlider:true});
    }

    handleClickVideo = e=>{
        this.props.setWatchVid(e);
        window.scrollTo(0,50); //scroll to top of the page
    }

    render(){
        return(
            <div id="slider">
                {this.props.filter.filterID ?
                    <div>
                        {this.props.language === "chn" ? <b>正在搜尋 : {this.props.filter.chn}</b>
                        : <b>Searching : {this.props.filter.eng}</b>
                        }
                        <b id="closeTag"><a onClick={e => this.props.resetFilter()}>&#x2715;</a></b>
                    </div>
                    : ''}
                {this.state.fetching ? 'Loading ... ' :
                 this.printVidGrid()}

                <div className="row">
                    <div className="col sliderBtn">
                        {this.state.showPageSlider ?
                        <RSlider
                            min={1}
                            max={this.state.lastPage}
                            step={1}
                            value={this.state.page}
                            orientation="horizontal"
                            onChange={this.handleOnChange}
                            onChangeComplete={this.handleComplete}
                        />
                         : '' }


                        {this.state.page !== 1 && <button className="pill btn accent" disabled={this.state.fetching} onClick={this.handleBackBtn}>Back</button> }
                        <button className="pill accent btn" id="pageNo" onClick={this.handleClickPage}>{this.state.page}</button>{/* use slider to change page number */}
                        {this.state.page !== this.state.lastPage && <button className="pill btn accent" disabled={this.state.fetching} onClick={this.handleNextBtn}>Next</button> }


                    </div>
                </div>



            </div>
        );
    }
}

function mapStateToProps(state){
    return{
        filter: state.filter,
        language: state.language.language
    }
}

export default connect(mapStateToProps,{setWatchVid,setFilterTag,setFilterActress,resetFilter})(Slider);

import {h} from 'preact';

const Footer = () =>(
    <div id="footer">
        <div className="card p1 bg-black">
            <p className="white ph2">Build version : 1.8</p>
        </div>
    </div>
)
export default Footer;
import { h, Component } from 'preact';

class Navbar extends Component{
    render(){
        return (
            <div className="row card" id="mainNav">
                <b className="col">HDAV</b>
                <div className="col">
                    <a onClick={()=>{}}>English</a>
                </div>
                <div className="col">
                    <a href="https://hdav.pl" onClick={()=>{}}>Old Site</a>
                </div>
                <div className="col">
                    <a href="https://gitlab.com/hdav/hdav">Gitlab</a>
                </div>
            </div>
        );
    }
}

export default Navbar;
import { h, Component } from 'preact';
import {connect} from 'preact-redux';
import axios from 'axios';
import Player from './player';

import {setFilterTag} from '../../actions/clickTag';
import {setFilterActress} from '../../actions/clickActress';

class Video extends Component{
    constructor(){
        super();
        this.state={
            videoInfo:{},
            videoURL: ''
        }
    }

    componentDidUpdate(prevProps){
        if(prevProps !== this.props){
            axios.get('https://hdav.pl/api/videoInfo?id='+this.props.video.videoID).then(res=>{
                this.setState({videoInfo:res.data,videoURL:res.data.url});
            });
        }
    }

    render(){
        return (
            this.state.videoURL ?
            <div className="row card" id="videoPlayerWrapper">
                <h4>{this.state.videoInfo.ctitle}</h4>
                <Player vidLink={this.state.videoURL} />
                <p><b>ID : </b><span className="inline-data inline-data-id">{this.state.videoInfo.idcode}</span></p>
                <p><b>發放日期 : </b><span className="inline-data inline-data-date">{this.state.videoInfo.date}</span></p>
                <p><b>女優 : </b>{this.state.videoInfo.actress.chn.map((name,i)=>(<a className="vidActress" actressid={this.state.videoInfo.actress.id[i]}
                onClick={e => this.props.setFilterActress(this.state.videoInfo.actress.id[i],this.state.videoInfo.actress.chn[i],this.state.videoInfo.actress.eng[i])}
                >{name}</a>))}</p>
                <p><b>標籤 : </b>{this.state.videoInfo.tags.chn.map((tag,i)=>(<a className="vidTags" tagid={this.state.videoInfo.tags.id[i]} 
                onClick={e => this.props.setFilterTag(this.state.videoInfo.tags.id[i],this.state.videoInfo.tags.chn[i],this.state.videoInfo.tags.eng[i])}
                >{tag}</a>))}</p>
            </div> : ''
        );
    }
}

function mapStateToProps(state){
    return{
        video: state.video
    }
}
export default connect(mapStateToProps,{setFilterTag,setFilterActress})(Video);

import {h}  from 'preact';


const Player = ({vidLink}) =>(
    <iframe title="videoPlayer" id="videoPlayer" src={vidLink} allowFullScreen={true} width="100%" height="100%" frameBorder={0}></iframe>
)




export default Player;